#-------------------------------------------------
#
# Project created by QtCreator 2011-11-22T23:56:11
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = dm_lab2
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

#QMAKE_CXXFLAGS += \
#    -march=native -O3

OTHER_FILES += \
    batch.sh

