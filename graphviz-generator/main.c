#include <stdio.h>

#define DOT_START_STR \
    "graph G {\n" \
	"size=\"20,20\"; resolution=128; bgcolor=\"#C6CFD532\";\n" \
    "        graph [ fontname = \"Anonymous Pro\",\n" \
    "                fontsize = 30,\n" \
    "                label = \"\\n\\n"
#define DOT_MIDDLE_STR \
    "\",\n" \
    "                size = \"6,6\" ];\n" \
    "        node [  shape = circle,\n" \
    "                color = black,\n" \
    "                fontname = \"Anonymous Pro\",\n" \
    "                fontsize = 25 ];\n" \
    "        edge [  color = black,\n" \
    "                style = filled,\n" \
    "                fontname = \"Helevetica-Outline\",\n" \
    "                fontsize = 25,\n" \
    "                fontcolor = black ];\n"
#define DOT_END_STR \
    "}\n"

int main(int argc, char **argv) {
    if (argc != 3) {
        printf("Usage: %s <in_file.txt> <out_file.dot>\n" \
               "where in_file.txt is something like\n" \
               "4\n1 2\n1 4\n2 3\n", argv[0]);
        return 0;
    }

    if (!freopen(argv[1], "r", stdin) || !freopen(argv[2], "w", stdout)) {
        printf("Are you sure that you specified correct input file name?\n");
        return 0;
    }

	int p, codeLen, code;
    if (scanf("%d%d%d", &p, &codeLen, &code) != 3) {
        return 0;
    }

    printf("%s%0*d%s", DOT_START_STR, codeLen, code, DOT_MIDDLE_STR);

    int x, y;
    while (scanf("%d%d", &x, &y) == 2) {
//        printf("	%d -- %d [ label = \"%d\" ];\n", x, y, w);
		printf("	%d -- %d [ ];\n", x, y);
    }

    printf("%s", DOT_END_STR);
    return 0;
}
