#include <QtCore/QCoreApplication>
#include <QVector>
#include <QMap>
#include <QFile>
#include <QDebug>
#include <algorithm>

template <typename T>
struct  Vertex
{
	Vertex() {}
	Vertex(T Label) : label(Label), degree(0), flag(false) {}
	~Vertex() {}
	
	T label;
	int degree;
	bool flag;
};

template <typename T>
struct Edge
{
	Edge() {}
	Edge(Vertex<T> newV1, Vertex<T> newV2) : v1(newV1), v2(newV2) {}
	~Edge() {}
	
	Vertex<T> v1, v2;
};

//template <typename T>
//struct Graph
//{
//	typedef QVector< Edge<T> > Type;
//};

template <typename T>
struct Graph
{
//	Graph() {}
//	Graph(int nVertex)
//	{
//		for (int i = 0; i < nVertex; i++)
//		{
//			v.push_back(Vertex<T>((T)i + 1));
//			el2v[(T)i + 1] = v.back();
//		}
//	}
//	~Graph() {}
	
	QVector< Edge<T> > e;
	QVector< Vertex<T> > v;
	QMap< T, Vertex<T> > el2v;
};


template <typename T>
class PruferSequence
{
public:
	PruferSequence() {}
	~PruferSequence()
	{
		code.clear();
	}
	void fromVector(QVector<T> newCode)
	{
		code = newCode;
	}
//	typename Graph<T>::Type unpack()
	Graph<T> unpack()
	{
		int nVertex = code.size() + 2;
		Graph<T> g(nVertex);
		QVector<T> unused;
		for (int i = 0; i < nVertex; i++)
		{
			unused.push_back(i + 1);
//			g.v[i].degree = 2;
//			g.v[i].flag = false;
		}
//		for (int i = 0; i < code.size(); i++)
//		{
//			g.el2v[code[i]].degree++;
//		}

//		foreach (T value, code)
//		{
//			foreach (Vertex<T> node, g.v)
//			{
//				if (value == node.label)
//					continue;

//				if (node.degree == 2)
//				{
//					g.e.push_back(Edge<T>(g.el2v[value], node));
//					g.el2v[value].degree--;
//					node.degree--;
//					break;
//				}

//			}
//		}
		
		for (int i = 0; i < code.size(); i++)
		{
			T v;
			for (int k = 0; k < unused.size(); k++)
			{
				bool ok = false;
				for (int j = i; j < code.size(); j++)
				{
					if (unused[k] == code[j])
						break;
					else if (j == code.size() - 1)
						ok = true;
				}
				if (ok)
				{
					v = unused[k];
					unused.remove(k);
					break;
				}
			}
			
			g.e.push_back(Edge<T>(v, code[i]));
		}
		
		// Two vertices will always remain unused
		g.e.push_back(Edge<T>(unused[0], unused[1]));
		
		return g;
	}
	
private:
	QVector<T> code;
};



//template <int Base>
template <typename T>
class BaseNumber
{
public:
	BaseNumber(T Base = 10, T MinDigit = 0, T InitialLen = 1)
	    : base(Base), minDigit(MinDigit), initialLen(InitialLen)
	{
		Q_ASSERT(base > 1);
		Q_ASSERT(minDigit >= (T)0);
		Q_ASSERT(initialLen > 0);
		
//        digits.push_back((T)0);
		for (int i = 0; i < InitialLen; i++)
		{
			digits.push_back(minDigit);
		}
	}
	~BaseNumber()
	{
		digits.clear();
	}
	QVector<T> getDigits()
	{
		return digits;
	}
	QString toString()
	{
		QString s;
		for (int i = digits.size() - 1; i >= 0; i--)
		{
			s.append(QString::number(digits[i]));
		}
		return s;
	}
	bool operator++(int)
	{
		bool done = false;
		for (int i = 0; i < digits.size(); i++)
		{
			if ((digits[i] == base - (T)1 && !done) || digits[i] == base)
			{
				if (i == digits.size() - 1)
				{
					digits[i] = minDigit;
					digits.push_back(minDigit + 1);
					return false; // break
				}
				else
				{
					digits[i] = minDigit;
					digits[i + 1] = digits[i + 1] + (T)1;
					done = true;
				}
			}
			else if (!done) // lowest digit increment
			{
				digits[i]++;
				return true; // break
			}
			else // done
			{
				return true; // break
			}
		}
		
//		return (digits.size() == initialLen);
//		return true;
	}
	
private:
	T base;
	T minDigit;
	QVector<T> digits;
	int initialLen;
};


template <typename T>
class LevelCode
{
public:
	LevelCode() {}
	LevelCode(QVector<T> new_code) : code(new_code) {}
	~LevelCode()
	{
		code.clear();
	}
	
	Graph<T> unpack(bool reverse)
	{
//		int nVertex = code.size();
		Graph<T> g;//(nVertex);
		
		if (reverse)
			std::reverse(code.begin(), code.end());
		
		T curLevel = code[0];
		int nChilds = 1;
		QMap< int, QVector<T> > cache;
		
		for (int i = 1; i < code.size(); i++)
		{
			T curCode = code[i];
			
			if (curCode < curLevel)
			{
				for (int j = 0; j < cache[curLevel].size(); j++)
					g.e.push_back(Edge<T>(i+1, cache[curLevel][j]));
				cache[curLevel].clear();
				
				for (int j = i - nChilds; j < i; j++)
					g.e.push_back(Edge<T>(i+1, j+1));
				
				nChilds = 1;
				curLevel = curCode;
			}
			else if (curCode > curLevel)
			{
				for (int j = i - nChilds; j < i; j++)
					cache[curLevel].push_back(j+1);
				
				curLevel = code[i];
				nChilds = 1;
			}
			else // curCode == curLevel
			{
				nChilds++;
			}
		}
		
		if (reverse)
			std::reverse(code.begin(), code.end());		
		
		return g;
	}
	QString toString()
	{
		QString s;
		for (int i = code.size() - 1; i >= 0; i--)
		{
			s.append(QString::number(code[i]));
		}
		return s;
	}
	
protected:
	QVector<T> code;
};

template <typename T>
class LevelCodeGenerator : public LevelCode<T>
{
public:
	LevelCodeGenerator(int nVertex) : L(LevelCode<T>::code), n(nVertex)
	{
		k = n / 2 + 1;
		
		for (int i = 1; i <= k; i++)
			L.push_back(i);
		for (int i = 2; i <= n - k + 1; i++)
			L.push_back(i);
		
		for (int i = 0; i < k; i++)
			W.push_back(i);
		W.push_back(1);
		for (int i = k + 1; i <= n - 1; i++)
			W.push_back(i);
		
		if (n == 4)
			p = 3;
		else
			p = n;
		
		q  = n - 1;
		h1 = k;
		h2 = n;
		r  = k;
		
		if (n % 2)
			c = MAX;
		else
			c = n + 1;
	}
	~LevelCodeGenerator()
	{
		W.clear();
	}
	QVector<T> getCode()
	{
		return L;
	}
	bool nextCode()
	{
//		LevelCode<T>::code.clear();
//		code.clear();
				
		if (q == 0)
			return false;
		
		bool fixit, needr, needc, needh2;
		T oldp, oldq, oldwq, delta;
		fixit = false;
		
		if ((c=n+1) or ((p=h2) and (((L[h1]=L[h2]+1) and (n-h2>r-h1))
		                           or ((L[h1]=L[h2]) and (n-h2+1<r-h1)))))
		{
			if (L[r]>3)
			{
				p= r; q= W[r];
				if (h1==r) h1=h1-1;
				fixit = true;
			}
			else
			{
				p=r; r=r-1; q=2;
			}
		}
		
		needr = false; needc = false; needh2 = false;
		if (p<=h1) h1= p-1;
		if (p<=r) needr = true;
		else if (p<=h2) needh2= true;
		else if ((L[h2]==L[h1]-1) && (n-h2==r-h1))
		{
			if (p<=c) needc = true;
		}
		else c = MAX;
		
		oldp=p; delta=q-p; oldq= L[q]; oldwq=W[q]; p=MAX;
		for (int i=oldp; i < n; i++)
		{
			L[i]=L[i+delta];
			if (L[i]==2) W[i]=1;
			else
			{
				p=i;
				if (L[i]==oldq) q = oldwq;
				else q= W[i+delta] - delta;
				W[i]= q;
			}
			if ((needr) && (L[i]==2))
			{
				needr=false; needh2=true; r= i-1;
			}
			if ((needh2) && (L[i]<=L[i-1]) && (i>r+1))
			{
				needh2=false; h2=i-1;
				if ((L[h2] == L[h1]-1) && (n-h2 == r-h1)) needc= true;
				else c= MAX;
			}
			if (needc)
			{
				if (L[i] != L[h1-h2+i]-1)
				{
					needc=false; c= i;
				}
				else
				{
					c= i+1;
				}
			}
		}	
		
#if 0		
// by lev
//		if (p < 0)
//			return false;
		
		bool f = false;
		
		if ((c == n + 1) || ((p == h2) && (((L[-1+h1] == L[-1+h2] + 1) && (n - h2 > r - h1)) ||
		                                   ((L[-1+h1] == L[-1+h2]) && (n - h2 + 1 < r - h1)))))
//		if ((c == n + 1) || ((p == h2) && ((((L[-1+h1] == L[-1+h2] + 1) && (n - h2 > r - h1)) ||
//		                                    ((L[-1+h1] == L[-1+h2]))) && (n - h2 + 1 < r - h1))))
		{
			if (L[-1+r] > 3)
			{
				p = r;
				q = W[-1+r];
				if (h1 == r)
					h1 = h1 - 1; // ?
				f = true;
			}
			else
			{
				p = r;
				r = r - 1;
				q = 2;
			}
		}
		
		bool need_r = false, need_c = false, need_h2 = false;
		
		if (p <= h1)
			h1 = p - 1;
		
		if (p <= r)
		{
			need_r = true;
		}
		else
		{
			if (p <= h2)
			{
				need_h2 = true;
			}
			else
			{
				if ((L[-1+h2] == L[-1+h1] - 1) && (n - h2 == r - h1))
				{
					if (p <= c)
						need_c = true;
				}
				else
				{
					c = MAX;
				}
			}
			
		}
//		else if (p <= h2)
//		{
//			need_h2 = true;
//		}
//		else if ((L[-1+h2] == L[-1+h1] - 1) && (n - h2 == r - h1))
//		{
//			if (p <= c)
//				need_c = true;
//		}
//		else
//		{
//			c = MAX;
//		}
		
		int old_p = p, delta = q - p, old_Lq = L[-1+q], old_Wq = W[-1+q];
		p = delta;
		
		for (int i = old_p; i <= n; i++)
		{
			L[-1+i] = L[-1+i + delta];
			if (L[-1+i] == 2)
			{
				W[-1+i] = 1;
			}
			else
			{
				p = i;
				if (L[-1+i] == old_Lq)
					q = old_Wq;
				else
					q = W[-1+i + delta] - delta;
				W[-1+i] = q;
			}
			
			if (need_r && (L[-1+i] == 2))
			{
				need_r = false;
				need_h2 = true;
				r = i - 1;
			}
			
			if (need_h2 && (L[-1+i] <= L[-1+i - 1]) && (i > r + 1))
			{
				need_h2 = false;
				h2 = i - 1;
				if ((L[-1+h2] == L[-1+h1] - 1) && (n - h2 == r - h1))
					need_c = true;
				else
					c = MAX;
			}
			if (need_c)
			{
				if (L[-1+i] != L[-1+h1 - h2 + i] - 1)
				{
					need_c = false;
					c = i;
				}
				else
				{
					c = i + 1;
				}
			}
		}
		
		// line 34
		if (f)
		{
			r = n - h1 + 1;
			
			for (int i = r + 1; i <= n; i++)
			{
				L[-1+i] = i - r + 1;
				W[-1+i] = i - 1;
			}
			W[-1+r + 1] = 1;
			h2 = n;
			p  = n;
			q  = p - 1;
			c  = MAX;
		}
		else if (c == MAX) //!
		{
			if (L[-1+old_p - 1] != 2) // ?
				p = old_p - 1;
			else
				p = old_p - 2;
			
			q = W[-1+p];	
		}
		
		if (need_h2)
		{
			h2 = n;
			if ((L[-1+h2] == L[-1+h1] - 1) && (h1 == r))
				c = n + 1;
			else
				c = MAX;
		}
#endif		
		return true;
	}
	
private:
	QVector<T>& L; // same as LevelCode<T>::code
	QVector<T> W;
	int n, k, p, q, h1, h2, c, r;
	
	enum { MAX = -999 };
};

template <typename T>
class FreeTreeGenerator
{
public:
	FreeTreeGenerator(T nVertex) : N(nVertex)
	{
		ub = N / 2;
		num = 0;
		
		par.resize(nVertex+1);
		par.fill(0);
		
		par[1] = 0;
		par[2] = 1;
	}
	~FreeTreeGenerator()
	{
		par.clear();
		allPar.clear();
	}
	
	void genAll()
	{
		gen(3, 0, 0, ub, 0, (N + 3)/2, 0, 0);
	}
	T getNumberOfTrees() { return N&1?num-1:num; }
	QVector< QString > getParentCodes()
	{
		QVector< QString > all;
		for (int k = 0; k < allPar.size(); k++)
		{
			QString s;
			for (T i = 1; i <= N; i++)
			{
				s.append(QString::number(allPar[k][i]));
			}
			all.push_back(s);
		}
		return all;
	}
	QVector< Graph<T> > unpack()
	{
		QVector< Graph<T> > all;
		for (int k = 0; k < allPar.size(); k++)
		{
			Graph<T> g;

			for (T i = 2; i <= N; i++)
			{
				T curNode = i;
				T parNode = allPar[k][i];
				g.e.push_back(Edge<T>(parNode, curNode));
			}

			all.push_back(g);
		}
		return all;
	}
	
	void printIt()
	{
		if (!num && N&1) {
			num = num+1;
			return;
		}
		num = num+1;
		
		printf("[%3lu]", N&1?num-1:num);
		for (T i = 1; i <= N; i++)
		{
			printf("%3lu", par[i]);
		}
		printf("\n");
				
		allPar.push_back(par);
	}
	
	void expand(T p, T h, T n)
	{
		if (N - p >= h)
		{
			gen(p+1,2,p-1,h,n,N,1,0);
		}
		if (((p-1)*2 >= N) || ((p-h-1==1) && (par[p]>2)) ||
			((p-h-1>=2) && ((par[h+2]>2) || (par[h+3]>2))))
		{
			gen(p+1,2,p-2,h,n,N,1,1);
		}
	}
	
	void gen(T p, T s, T cL, T h, T l, T n, T f, T g)
	{
		if (p > n)
		{
			if (f == 0) expand(p-1,h,n);
			else printIt();	
		}
		else
		{
			if ((cL == 0) && (p<=ub+2))
			    par[p] = p-1;
			else if (par[p-cL] < s)
				par[p]=par[p-cL];
			else
			{
				par[p] = cL + par[p-cL];
				if (g==1)
				{
					if (((l-1)*2 < n)
					    && (p-cL<=l) && (
					        ((p-cL+1<l) &&
					         (par[p-cL+1]==2)
					         && (p-cL+2<=l) && (par[p-cL+2]==2))  // {case 1}
					        || ((p-cL+1==l) && (par[p-cL+1]==2)) // {case 2}
					        || (p-cL+1>l))) // {case 3}
					{
						s = par[p]; cL = p-s;
						par[p] = par[par[p]];
					}
					else if (par[p-cL]==2)
						par[p]=1;
				}
			}
			gen(p+1, s, cL,h,l,n,f,g);
			while ((par[p] > 2) && ((f==0) || (p>l+h-g)))
			{
				if (s==0)
			       h = p-2;
				s = par[p]; par[p] = par[s];
				if (f==0) gen(p+1,s,p-s,h,0,N-h+1,f,g);
				else gen(p+1,s,p-s,h,l,n,f,g);
			}
			if (f == 0) expand(p-1,h,p-1);
		}
	}
	
private:
	T N;			// number of nodes in a tree
	QVector<T> par;	// parent position of i
	T num;			// total number of trees
	T ub;			// upper bound
	QVector< QVector<T> > allPar;
};



int main(int argc, char *argv[])
{
//    QCoreApplication a(argc, argv);
	int nVertex;
	
	if (argc != 2)
	{
		printf("Usage: %s <num_stations>\n", argv[0]);
		return -1;
	}
	
	bool ok;
	nVertex = QString(argv[1]).toInt(&ok);
	if (!ok)
	{
		printf("Usage: %s <num_stations>\n", argv[0]);
		return -1;
	}
	Q_ASSERT(nVertex >= 3);
	
	
//	QVector<int> tmp(4,4,3,3,2,3,3,2,1);
//	int v[] = {4,4,3,3,2,3,3,2,2,1};
//	int v[] = {5,5,4,4,3,4,4,3,4,3,2,3,3,2,3,3,3,2,3,3,2,2,1};
//	QVector<int> t;
//	for (int i = 0; i < int(sizeof(v)/sizeof(v[0])); i++)
//		t.push_back(v[i]);
//	LevelCode<int> c(t);

//	Graph<int> g;
//	g = c.unpack();

//	freopen("out.txt", "w", stdout);
//	printf("%d\n", int(sizeof(v)/sizeof(v[0])));
//	for (int i = 0; i < g.e.size(); i++)
//	{
//		printf("%d %d\n", g.e[i].v1.label, g.e[i].v2.label);
//	}

#if 1
	FreeTreeGenerator<__uint64_t> gen(nVertex);
	
	gen.genAll();
	
	__uint64_t nTrees = gen.getNumberOfTrees();
	printf("Number of free trees with %d vertices = %lu\n", nVertex, nTrees);
	
	QVector< Graph<__uint64_t> > trees = gen.unpack();
	QVector< QString > parCodes = gen.getParentCodes();
	
	for (__uint64_t treeIndex = 0; treeIndex < nTrees; treeIndex++)
	{
		QFile file(QString("luna3/%1/%2.txt").arg(nVertex).arg(treeIndex+1, nVertex / 3, 10, QChar('0')));
		if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
			return -2;
		
		QTextStream out(&file);
		out << nVertex << "\n";
		out << parCodes[treeIndex] << "\n";
		
		for (int i = 0; i < trees[treeIndex].e.size(); i++)
		{
			out << trees[treeIndex].e[i].v1.label << " " << trees[treeIndex].e[i].v2.label << "\n";
		}
		
		file.close();	
	}
	
#endif
	
#if 0
	LevelCodeGenerator<int> c(nVertex);	
	Graph<int> g;
//	g = c.unpack(true);

//	freopen("out.txt", "w", stdout);
//	printf("%d\n", nVertex);
//	printf("%s\n", c.toString().toAscii().data());
//	for (int i = 0; i < g.e.size(); i++)
//	{
//		printf("%d %d\n", g.e[i].v1.label, g.e[i].v2.label);
//	}
	
	int treeIndex = 1;
	while (1)
	{
		g = c.unpack(true);
		
		QFile file(QString("luna2/%1/%2.txt").arg(nVertex).arg(treeIndex, nVertex - 2, 10, QChar('0')));
		if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
			return -2;
		
		QTextStream out(&file);
		out << nVertex << "\n";
		out << c.toString() << "\n";
		
		for (int i = 0; i < g.e.size(); i++)
		{
			out << g.e[i].v1.label << " " << g.e[i].v2.label << "\n";
			//			printf("%d <-> %d\n", g.e[i].v1.label, g.e[i].v2.label);
		}
		
		file.close();
		treeIndex++;
		
		if (!c.nextCode())
			return 0;
	}
#endif
	
	
#if 0
	int codeLen = nVertex - 2;
	BaseNumber<int> number(nVertex + 1, 1, codeLen);
	PruferSequence<int> seq;
	Graph<int> g;
	
	int treeIndex = 1;
	while (1)
	{
		seq.fromVector(number.getDigits());
		g = seq.unpack();
		
//		printf("******************************\n");
		
		QFile file(QString("luna/%1/%2.txt").arg(nVertex).arg(treeIndex, nVertex - 2, 10, QChar('0')));
		if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
			return -2;
		
		QTextStream out(&file);
		out << nVertex << "\n";
		out << number.toString() << "\n";
		
		for (int i = 0; i < g.e.size(); i++)
		{
			out << g.e[i].v1.label << " " << g.e[i].v2.label << "\n";
//			printf("%d <-> %d\n", g.e[i].v1.label, g.e[i].v2.label);
		}
		
		file.close();
		treeIndex++;
		
		if (!number++)
			break;
	}
	
//    for (int i = 0; i < 100; i++)
//    {
//		printf(number.toString().toAscii() + "\n");
//        number++;
//    }

//	printf(number.toString().toAscii() + "\n");
//	while (number++)
//		printf(number.toString().toAscii() + "\n");
#endif
	
	return 0;
//    return a.exec();
}

