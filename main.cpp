#include <QVector>
#include <QFile>
#include <QDebug>
#include <algorithm>

template <typename T>
struct  Vertex
{
	Vertex() {}
	Vertex(T Label) : label(Label), degree(0), flag(false) {}
	~Vertex() {}
	
	T label;
	int degree;
	bool flag;
};

template <typename T>
struct Edge
{
	Edge() {}
	Edge(Vertex<T> newV1, Vertex<T> newV2) : v1(newV1), v2(newV2) {}
	~Edge() {}
	
	Vertex<T> v1, v2;
};

template <typename T>
struct Graph
{
//	Graph() {}
//	Graph(int nVertex)
//	{
//		for (int i = 0; i < nVertex; i++)
//		{
//			v.push_back(Vertex<T>((T)i + 1));
//			el2v[(T)i + 1] = v.back();
//		}
//	}
//	~Graph() {}
	
	QVector< Edge<T> > e;
	QVector< Vertex<T> > v;
	QMap< T, Vertex<T> > el2v;
};


template <typename T>
class PruferSequence
{
public:
	PruferSequence() {}
	~PruferSequence()
	{
		code.clear();
	}
	void fromVector(QVector<T> newCode)
	{
		code = newCode;
	}
	Graph<T> unpack()
	{
		int nVertex = code.size() + 2;
		Graph<T> g;
		QVector<T> unused;
		for (int i = 0; i < nVertex; i++)
			unused.push_back(i + 1);
	
		for (int i = 0; i < code.size(); i++)
		{
			T v;
			for (int k = 0; k < unused.size(); k++)
			{
				bool ok = false;
				for (int j = i; j < code.size(); j++)
				{
					if (unused[k] == code[j])
						break;
					else if (j == code.size() - 1)
						ok = true;
				}
				if (ok)
				{
					v = unused[k];
					unused.remove(k);
					break;
				}
			}
			
			g.e.push_back(Edge<T>(v, code[i]));
		}
		
		// Two vertices will always remain unused
		g.e.push_back(Edge<T>(unused[0], unused[1]));
		
		return g;
	}
	
private:
	QVector<T> code;
};


template <typename T>
class BaseNumber
{
public:
	BaseNumber(T Base = 10, T MinDigit = 0, T InitialLen = 1)
	    : base(Base), minDigit(MinDigit), initialLen(InitialLen)
	{
		Q_ASSERT(base > 1);
		Q_ASSERT(minDigit >= (T)0);
		Q_ASSERT(initialLen > 0);
		
		for (int i = 0; i < InitialLen; i++)
			digits.push_back(minDigit);
	}
	~BaseNumber()
	{
		digits.clear();
	}
	QVector<T> getDigits()
	{
		return digits;
	}
	QString toString()
	{
		QString s;
		for (int i = digits.size() - 1; i >= 0; i--)
		{
			s.append(QString::number(digits[i]));
		}
		return s;
	}
	bool operator++(int)
	{
		bool done = false;
		for (int i = 0; i < digits.size(); i++)
		{
			if ((digits[i] == base - (T)1 && !done) || digits[i] == base)
			{
				if (i == digits.size() - 1)
				{
					digits[i] = minDigit;
					digits.push_back(minDigit + 1);
					return false; // break
				}
				else
				{
					digits[i] = minDigit;
					digits[i + 1] = digits[i + 1] + (T)1;
					done = true;
				}
			}
			else if (!done) // lowest digit increment
			{
				digits[i]++;
				return true; // break
			}
			else // done
			{
				return true; // break
			}
		}
		
		return true; //(digits.size() == initialLen)
	}
	
private:
	T base;
	T minDigit;
	QVector<T> digits;
	int initialLen;
};


template <typename T>
class FreeTreeGenerator
{
public:
	FreeTreeGenerator(T nVertex) : N(nVertex)
	{
		ub = N / 2;
		num = 0;
		
		par.resize(nVertex+1);
		par.fill(0);
		
		par[1] = 0;
		par[2] = 1;
	}
	~FreeTreeGenerator()
	{
		par.clear();
		allPar.clear();
	}
	
	void genAll()
	{
		gen(3, 0, 0, ub, 0, (N + 3)/2, 0, 0);
	}
	T getNumberOfTrees() { return N&1?num-1:num; }
	QVector< QString > getParentCodes()
	{
		QVector< QString > all;
		for (int k = 0; k < allPar.size(); k++)
		{
			QString s;
			for (T i = 1; i <= N; i++)
			{
				s.append(QString::number(allPar[k][i]));
			}
			all.push_back(s);
		}
		return all;
	}
	QVector< Graph<T> > unpack()
	{
		QVector< Graph<T> > all;
		for (int k = 0; k < allPar.size(); k++)
		{
			Graph<T> g;
			for (T i = 2; i <= N; i++)
			{
				T curNode = i;
				T parNode = allPar[k][i];
				g.e.push_back(Edge<T>(parNode, curNode));
			}
			all.push_back(g);
		}
		return all;
	}
	void printIt()
	{
		if (!num && N&1) {
			num = num+1;
			return;
		}
		num = num+1;
		
		printf("[%3lu]", N&1?num-1:num);
		for (T i = 1; i <= N; i++)
		{
			printf("%3lu", par[i]);
		}
		printf("\n");
				
		allPar.push_back(par);
	}
	void expand(T p, T h, T n)
	{
		if (N - p >= h)
		{
			gen(p+1,2,p-1,h,n,N,1,0);
		}
		if (((p-1)*2 >= N) || ((p-h-1==1) && (par[p]>2)) ||
			((p-h-1>=2) && ((par[h+2]>2) || (par[h+3]>2))))
		{
			gen(p+1,2,p-2,h,n,N,1,1);
		}
	}
	void gen(T p, T s, T cL, T h, T l, T n, T f, T g)
	{
		if (p > n)
		{
			if (f == 0) expand(p-1,h,n);
			else printIt();	
		}
		else
		{
			if ((cL == 0) && (p<=ub+2))
			    par[p] = p-1;
			else if (par[p-cL] < s)
				par[p]=par[p-cL];
			else
			{
				par[p] = cL + par[p-cL];
				if (g==1)
				{
					if (((l-1)*2 < n)
					    && (p-cL<=l) && (
					        ((p-cL+1<l) &&
					         (par[p-cL+1]==2)
					         && (p-cL+2<=l) && (par[p-cL+2]==2))  // {case 1}
					        || ((p-cL+1==l) && (par[p-cL+1]==2)) // {case 2}
					        || (p-cL+1>l))) // {case 3}
					{
						s = par[p]; cL = p-s;
						par[p] = par[par[p]];
					}
					else if (par[p-cL]==2)
						par[p]=1;
				}
			}
			gen(p+1, s, cL,h,l,n,f,g);
			while ((par[p] > 2) && ((f==0) || (p>l+h-g)))
			{
				if (s==0)
			       h = p-2;
				s = par[p]; par[p] = par[s];
				if (f==0) gen(p+1,s,p-s,h,0,N-h+1,f,g);
				else gen(p+1,s,p-s,h,l,n,f,g);
			}
			if (f == 0) expand(p-1,h,p-1);
		}
	}
	
private:
	T N;			// number of nodes in a tree
	QVector<T> par;	// parent position of i
	T num;			// total number of trees
	T ub;			// upper bound
	QVector< QVector<T> > allPar;
};



int main(int argc, char *argv[])
{
	int nVertex;
	
	if (argc != 2)
	{
		printf("Usage: %s <num_stations>\n", argv[0]);
		return -1;
	}
	
	bool ok;
	nVertex = QString(argv[1]).toInt(&ok);
	if (!ok)
	{
		printf("Usage: %s <num_stations>\n", argv[0]);
		return -1;
	}
	if (nVertex < 3)
	{
		printf("At least three stations!\n");
		return -1;
	}
	
#if 1
	FreeTreeGenerator<__uint64_t> gen(nVertex);
	
	gen.genAll();
	
	__uint64_t nTrees = gen.getNumberOfTrees();
	printf("Number of free trees with %d vertices = %lu\n", nVertex, nTrees);
	
	QVector< Graph<__uint64_t> > trees = gen.unpack();
	QVector< QString > parCodes = gen.getParentCodes();
	
	for (__uint64_t treeIndex = 0; treeIndex < nTrees; treeIndex++)
	{
		QFile file(QString("luna3/%1/%2.txt").arg(nVertex).arg(treeIndex+1, nVertex / 3, 10, QChar('0')));
		if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
			return -2;
		
		QTextStream out(&file);
		out << nVertex << "\n";
		out << nVertex << "\n";
		out << parCodes[treeIndex] << "\n";
		
		for (int i = 0; i < trees[treeIndex].e.size(); i++)
		{
			out << trees[treeIndex].e[i].v1.label << " " << trees[treeIndex].e[i].v2.label << "\n";
		}
		
		file.close();	
	}
	
#else
	int codeLen = nVertex - 2;
	BaseNumber<int> number(nVertex + 1, 1, codeLen);
	PruferSequence<int> seq;
	Graph<int> g;
	
	int treeIndex = 1;
	while (1)
	{
		seq.fromVector(number.getDigits());
		g = seq.unpack();

		QFile file(QString("luna4/%1/%2.txt").arg(nVertex).arg(treeIndex, nVertex - 2, 10, QChar('0')));
		if (!file.open(QIODevice::WriteOnly | QIODevice::Text))
			return -2;
		
		QTextStream out(&file);
		out << nVertex << "\n";
		out << codeLen << "\n";
		out << number.toString() << "\n";
		
		for (int i = 0; i < g.e.size(); i++)
		{
			out << g.e[i].v1.label << " " << g.e[i].v2.label << "\n";
		}
		
		file.close();
		treeIndex++;
		
		if (!number++)
			break;
	}
	
//    for (int i = 0; i < 100; i++)
//    {
//		printf(number.toString().toAscii() + "\n");
//        number++;
//    }

//	printf(number.toString().toAscii() + "\n");
//	while (number++)
//		printf(number.toString().toAscii() + "\n");
#endif
	
	return 0;
}

